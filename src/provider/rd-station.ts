import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from '../app/app.environment';


@Injectable()
export class RdStationProvider {

  constructor(public http: HttpClient) {

  }

  public send(item:any){
    item = {
      "token_rdstation": environment.rdStation.token,
      "identificador": environment.rdStation.name,
      "email": item.email,
      "Nome": item.nome,
      "Telefone": item.telefone,
      "Estado": item.estado,
      "Profissão": item.profissao,
      "Estado Civil": item.estado_civil,
      "Lote": item.lote,
      "Valor Atual": item.atual,
      "Valor Entrada": item.entrada,
      "Taxa": item.taxa,
      "Meses": item.mes,
      "Valor Presente": item.valorPresente,
      "Pagamento": item.pagamento,
    }
    console.log(item);
    return this.http.post(
      environment.rdStation.url,
      item,
      {
        headers: { 'Content-Type': 'application/json; charset=utf-8'}
      }
    );
  }
}
