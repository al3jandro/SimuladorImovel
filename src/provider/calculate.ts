import { Injectable } from '@angular/core';

@Injectable()
export class CalculateProvider {
  /**
   * ENT Valor de la entrada
  */
  ENT:number = 3;
  constructor() {
    //console.log('Hello CalculateProvider Provider');
  }

  /**
   * @vFin Float valor inicial financiamiento
   */
  public calc(item:any, dados:any){
    let taz:any = this.taxas(dados.mes, dados.lote);
    let taz2 = `${taz * 100}%`;
    let vP = this.round(dados.atual - dados.entrada,2);
    let vF = this.vF(vP, taz, this.ENT);
    let pgto = this.pGTO(taz, dados.mes, vF);
    let total_parcelas = this.totalParcelas(dados.mes, pgto);
    let total_geral = this.totalGeral(total_parcelas, dados.entrada);
    let datos = {
      lote: dados.lote,
      valorPresente: vP,
      valorFuturo: vF,
      pagamento: pgto,
      totalParcelas: total_parcelas,
      entrada: dados.entrada,
      atual: dados.atual,
      taxa: taz2,
      mes: dados.mes,
      totalGeral: total_geral,
      nome: item.nome,
      email: item.email,
      telefone: item.telefone,
      estado: item.estado,
      profissao: item.profissao,
      estado_civil: item.civil,
      link: dados.image
    };
    return datos;
  }

  private totalGeral(a:any, b:any){
    let total = parseInt(a) + parseInt(b);
    console.log('Total Geral', total)
    return this.round(total,2);
  }


  private taxas(mes:number, lote:any){
    let taxa = '';
    if(lote === 'Mosaico Ponta Negra'){
      if(mes < 37){
        taxa = this.taxa[0].taxM;
      } else if(mes < 43){
        taxa = this.taxa[1].taxM;
      } else if(mes < 61){
        taxa = this.taxa[2].taxM;
      } else{
        taxa = this.taxa[3].taxM;
      }
    } else if(lote === 'Mosaico Essence'){
      if(mes == 48){
        taxa = this.taxaE[0].taxM;
      } else if(mes == 60){
        taxa = this.taxaE[1].taxM;
      } else if(mes == 120){
        taxa = this.taxaE[2].taxM;
      }
    } else if(lote === 'Vintage') {
      taxa = this.taxaE[1].taxM;
    }
    return taxa;
  }

  /**
   *
   * @param vp valor presente
   * @param i taxa
   * @param n prazos
   */
  public vF(vp:number, i:any, n:number){
    let dos = 1+i;
    let tres = Math.pow(dos, n-1);
    let vf = vp * tres;
    return this.round(vf, 2);
  }

  /**
   *
   * @param i taxa
   * @param mes meses juros
   * @param vF valor futuro
   */
  public pGTO(i:any, mes:number, vF:any){
    let cF = i/(1-Math.pow(1+i,-mes));
    let pgto = vF * cF;
    return this.round(pgto,2);
  }

  /**
   *
   * @param mes
   * @param pgto
   */
  private totalParcelas(mes:number, pgto:any){
    return this.round(mes*pgto,2);
  }

  /**
   *
   * @param value
   * @param precision
   */
  public round(value, precision) {
    var multiplier = Math.pow(10, precision || 0);
    return Math.round(value * multiplier) / multiplier;
  }

  /**
   *
   * @param vEntrada
   * @param vAtual
   */
  public porcentage(vEntrada, vAtual){
    let porcentage = vEntrada/vAtual;
    return this.round(porcentage,4);
  }


  taxa:any = [
    { id:1, mes: 36,  taxM: 0,        taxA: 0       },
    { id:2, mes: 42,  taxM: 0.005,    taxA: 0.00617 },
    { id:3, mes: 60,  taxM: 0.007207, taxA: 0.0009  },
    { id:4, mes: 120, taxM: 0.009489, taxA: 0.0012  }
  ];

  taxaE:any = [
    { id:1, mes: 48,  taxM: 0.00083  },
    { id:2, mes: 60,  taxM: 0.002466 },
    { id:3, mes: 120, taxM: 0.009489 }
  ];

  /**
   * ponto a virgula
   */
  virgula(num){
    if (!isNaN(num)) {
      num = num.toString().replace(/\./g, ',');
      num = num.toString().split('').reverse().join('').replace(/(?=\d*\.?)(\d{3})/g, '$1.');
      num = num.split('').reverse().join('').replace(/^[\.]/, '');
    }
    return num;
  }

  reverse(item){
    if(item != null) {
      item = item.toString().replace(/\./g, "");
      item = item.toString().replace(/\,/g, ".");
    }
    return item;
  }
}
