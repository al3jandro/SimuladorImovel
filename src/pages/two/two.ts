import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ModalController, AlertController, LoadingController } from 'ionic-angular';
import { CalculateProvider } from "./../../provider/calculate";


@IonicPage()
@Component({
  selector: "page-two",
  templateUrl: "two.html"
})
export class TwoPage {
  dados: any;
  sim: any = { lote: "", mes: "", atual: "", entrada: "", image: "" };
  meses: any;
  numLote: any;
  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public loading: LoadingController,
    public modal: ModalController,
    public alert: AlertController,
    private calc: CalculateProvider
  ) {
    this.dados = this.navParams.get("res");
  }

  onSelect(items) {
    //console.log(items);
    const resultado = this.propiedade.find(lote => lote.name === items);
    this.sim.atual = this.calc.virgula(resultado.valor);
     //console.log('onSelect', this.sim.entrada);
    if (items === "Mosaico Essence") {
      this.numLote = this.round(resultado.valor * 0.15, 0);
      this.sim.entrada = this.calc.virgula(this.round(resultado.valor * 0.15, 0));
      this.sim.image = "essence";
      this.meses = this.essence_mes;
      this.sim.mes = 120;
    } else if (items === "Mosaico Ponta Negra") {
      this.numLote = this.round(resultado.valor * 0.1, 0);
      this.sim.entrada = this.calc.virgula(this.round(resultado.valor * 0.1, 0));
      this.meses = this.pontanegra_mes;
      this.sim.image = "pontanegra";
      this.sim.mes = 120;
    } else if (items === "Vintage") {
      this.numLote = this.round(resultado.valor * 0.3, 0);
      this.sim.entrada = this.calc.virgula(this.round(resultado.valor * 0.3, 0));
      this.meses = this.vintage_mes;
      this.sim.image = "vintage";
      this.sim.mes = 60;
    }
  }

  onValidateEntrada(lote: string) {
    let per;
    if (lote === 'Mosaico Ponta Negra') {
      per = 10;
    } else if (lote === 'Mosaico Essence') {
      per = 15;
    } else {
      per = 30;
    }

    this.alert
      .create({
        title: "Operação Inválida<hr />",
        subTitle: `O valor de entrada deve ser igual ou maior que ${per}% do valor total. 
          Mas calma, podemos parcelar isso pra caber no seu orçamento.`,
        buttons: ["Ok"]
      })
      .present();
  }

  pontanegra_mes: any = [
    { id: 6, name: 6 },
    { id: 12, name: 12 },
    { id: 36, name: 36 },
    { id: 42, name: 42 },
    { id: 48, name: 48 },
    { id: 54, name: 54 },
    { id: 60, name: 60 },
    { id: 66, name: 66 },
    { id: 72, name: 72 },
    { id: 78, name: 78 },
    { id: 84, name: 84 },
    { id: 90, name: 90 },
    { id: 96, name: 96 },
    { id: 102, name: 102 },
    { id: 108, name: 108 },
    { id: 114, name: 114 },
    { id: 120, name: 120 }
  ];
  essence_mes: any = [
    { id: 48, name: 48 },
    { id: 60, name: 60 },
    { id: 120, name: 120 }
  ];

  vintage_mes: any = [
    { id: 60, name: 60 }
  ];

  propiedade: any = [
    { name: "Mosaico Ponta Negra", valor: 140000.0 },
    { name: "Mosaico Essence", valor: 180000.0 },
    { name: "Vintage", valor: 326120.19 }
  ];

  next() {
    console.log('Ponto', this.sim.entrada.search("."));
    this.sim.entrada = this.calc.reverse(this.sim.entrada);
    this.sim.atual = this.calc.reverse(this.sim.atual);
    //console.log('Entrada', this.sim.entrada);
    let load = this.loading.create({
      spinner: "dots",
      content: "Carregando",
      duration: 2000
    });
    load.present();
    console.log('Lote', this.numLote);
    if (this.sim.entrada > this.numLote || this.sim.entrada == this.numLote) {
      this.navCtrl.push("OnePage", { res: this.sim });
      //console.log("sim", this.sim);
      load.dismiss();
    } else {
      this.onValidateEntrada(this.sim.lote);
    }
  }

  /**
   *
   * @param value
   * @param precision
   */
  private round(value, precision) {
    var multiplier = Math.pow(10, precision || 0);
    return Math.round(value * multiplier) / multiplier;
  }
}
