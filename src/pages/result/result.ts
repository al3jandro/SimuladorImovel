import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, AlertController, LoadingController } from 'ionic-angular';
import { CalculateProvider } from "./../../provider/calculate";


@IonicPage()
@Component({
  selector: "page-result",
  templateUrl: "result.html"
})
export class ResultPage {
  res: any;
  link: any;
  url: string;

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public alertCtrl: AlertController,
    private cal: CalculateProvider,
    public loading: LoadingController
  ) {
    this.res = this.navParams.get("res");
    console.log("two", this.res);
    this.link = this.navParams.get("link");
    console.log("link", this.link);
    this.res.atual = this.cal.virgula(this.res.atual);
    this.res.entrada = this.cal.virgula(this.res.entrada);
    this.res.valorPresente = this.cal.virgula(this.res.valorPresente);
    this.res.pagamento = this.cal.virgula(this.res.pagamento);
    console.log('Lote => ', this.res.lote);
    if(this.res.lote === 'Mosaico Ponta Negra') {
      this.url = 'https://conteudo.projetomosaico.com.br/agendamento-mosaico-ponta-negra';
    } else if(this.res.lote === 'Mosaico Essence') {
      this.url = 'https://conteudo.projetomosaico.com.br/agendamento-mosaico-essence';
    } else {
      this.url = 'https://conteudo.projetomosaico.com.br/falar-com-consultor-projeto-mosaico'
    }
  }

  corretor(item) {
    console.log(item);
    //return location.href = `http://${item}.projetomosaico.com.br/`;
  }

  ionViewDidLoad() {
    console.log("ionViewDidLoad ResultPage");
  }

  nextPage() {
    let alert = this.alertCtrl.create({
      title: "Atencion",
      subTitle: "Você tem certeza que quer refazer esta simulação?",
      buttons: [
        {
          text: "NÃO",
          role: "cancel",
          handler: () => {}
        },
        {
          text: "SIM",
          handler: () => {
            this.navCtrl.setRoot("TwoPage");
          }
        }
      ]
    });
    alert.present();
  }
  prev() {
    let load = this.loading.create({
      spinner: 'dots',
      content: 'Carregando'
    });
    load.present();
    this.navCtrl.setRoot("TwoPage").then(()=>{
      load.dismiss();
    });
  }
}
