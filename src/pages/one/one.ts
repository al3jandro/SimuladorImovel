import { Component } from '@angular/core';
import { IonicPage, NavController, ModalController, NavParams, LoadingController} from 'ionic-angular';
import { FormBuilder, Validators } from '@angular/forms';
import { ValidateProvider } from '../../provider/validate';
import { CalculateProvider } from "./../../provider/calculate";
import { RdStationProvider } from "./../../provider/rd-station";


@IonicPage()
@Component({
  selector: "page-one",
  templateUrl: "one.html"
})


export class OnePage {
  itemsForm: any;
  sim: any;
  dados: any;
  
  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public modal: ModalController,
    public loading: LoadingController,
    private builder: FormBuilder,
    private cal: CalculateProvider,
    private rdStation: RdStationProvider
  ) {
    this.sim = this.navParams.get('res');
    this.itemsForm = this.builder.group({
      nome: [
        "",
        [
          Validators.required,
          ValidateProvider.requerid
        ]
      ],
      email: ["", [Validators.required, ValidateProvider.emailValidator]],
      telefone: ["", [Validators.required, ValidateProvider.telefone]],
      profissao: ["", [Validators.required]],
      estado: ["", [Validators.required]],
      civil: ["", [Validators.required]]
    });
  }

  ionViewDidLoad() {}

  modalInfo() {
    this.modal.create("PoliticaPage").present();
  }

  send() {
    let load = this.loading.create({
      spinner: 'dots',
      content: 'Simulando',
      duration: 2000
    });
    load.present();
    if (!this.itemsForm.valid) {
      //console.log("Falta");
    } else {
      this.dados = this.itemsForm.value;
      //console.log('sim', this.sim);
      //console.log('dados', this.dados);
      let result = this.cal.calc(this.dados, this.sim);
      //console.log(result);
      //this.navCtrl.setRoot('ResultPage', { res: result, link: this.sim.image });
      this.rdStation.send(result).subscribe(() => {
        this.navCtrl.setRoot('ResultPage', { res: result, link: this.sim.image });
        console.log('Sucesso', result);
        load.dismiss();
      }, error => {
        console.log('Erro', error);
      })
    }

  }

  prev() {
    let load = this.loading.create({
      spinner: 'dots',
      content: 'Carregando'
    });
    load.present();
    this.navCtrl.setRoot("TwoPage").then(()=>{
      load.dismiss();
    });
  }

  state:any = [
    { name: 'Acre' }, { name: 'Alagoas' },{ name: 'Amapá' },
    { name: 'Amazonas' }, { name: 'Bahia' }, { name: 'Ceará' },
    { name: 'Distrito Federal' }, { name: 'Espírito Santo' }, { name: 'Goiás' },
    { name: 'Maranhão' }, { name: 'Mato Grosso' }, { name: 'Mato Grosso do Sul' },
    { name: 'Pará' }, { name: 'Paraíba' }, { name: 'Paraná' },
    { name: 'Pernambuco' }, { name: 'Piauí' }, { name: 'Rio de Janeiro' },
    { name: 'Rio Grande do Norte' }, { name: 'Rio Grande do Sul' }, { name: 'Rondônia' },
    { name: 'Roraima' }, { name: 'Santa Catarina' }, { name: 'São Paulo' },
    { name: 'Sergipe' }, { name: 'Tocantins' } 
  ];

  estado_civil: any = [
    { name: 'Solteiro(a)'}, { name: 'Casado(a)'},{ name: 'Viúdo(a)'},
  ]
}
