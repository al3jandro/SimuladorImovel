import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { OnePage } from './one';
import { BrMaskerModule } from 'brmasker-ionic-3';


@NgModule({
  declarations: [
    OnePage,
  ],
  imports: [
    IonicPageModule.forChild(OnePage),
    BrMaskerModule
  ],
})
export class OnePageModule {}
